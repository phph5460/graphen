/**
 * Die Elternklasse aller Algorithmen. Hier werden standard Methoden und Eigenschaften definiert.
 */
public abstract class Algorithmus {
  
  protected final List<Knoten> knoten;
  protected ArrayList<Knoten> open;
  protected ArrayList<Knoten> closed;
  protected ArrayList<Knoten> finalerWeg;

  protected HashMap<Knoten, Knoten> vorgaenger;
  // für Schleifendurchgänge des Algorithmus
  protected int durchgang;
  // Zeitmessung in Nanosekunde
  protected long startNanos, dauerNanos;

  
  public Algorithmus(ArrayList<Knoten> _knoten) {
    this.knoten = _knoten;
  }

  /**
   * Startet den Algorithmus
   * @param start Startpunkt
   * @param ziel Zielpunkt
   */
  abstract void starteAlgorithmus(Knoten start, Knoten ziel);

  /**
   * Ermittelt alle Nachbarn des aktuellen Knotens und speichert sie in die Open-Liste.
   * @param aktuellerKnoten Pivot-Punkt, um seine Nachbarn zu ermitteln
   */
  abstract void sucheNachfolger(Knoten aktuellerKnoten, Knoten ziel);

  /**
   * Vergleicht verfügbare Knoten in der Open-Liste und wertet die Kosten aus, sodass der Knoten mit dem kleinsten Kantengewicht zurückgegeben wird.
   * @param listeMitKnoten Open-Liste wird mitgegeben
   * @param _kosten Knoten mit ihren Kosten
   * @return Gibt den Knoten mit dem kleinsten Kantengewicht zurück.
   */
  protected Knoten minimalesKantengewicht(ArrayList<Knoten> listeMitKnoten, Map<Knoten, Float> _kosten) {
    float zwischenGewicht = Float.MAX_VALUE;
    Knoten knoten = null;

    for (Knoten aktuellerKnoten : listeMitKnoten) {
      if (_kosten.get(aktuellerKnoten) < zwischenGewicht) {
        zwischenGewicht = _kosten.get(aktuellerKnoten);
        knoten = aktuellerKnoten;
      }
    }
    return knoten;
  }

  /**
   * Erstelle den Weg zum Ziel mit Hilfe der Vorgänger-Liste und speichert die Knoten in die finalerWeg-Liste.
   * @param _vorgaenger Die Liste mit allen gelaufenen Vorgängern.
   * @param ziel Gibt den Zielknoten an.
   */
  protected void wegErstellen(HashMap<Knoten, Knoten> _vorgaenger, Knoten ziel) {
    if (ziel != null) {
      this.finalerWeg.add(ziel);
      this.wegErstellen(_vorgaenger, _vorgaenger.get(ziel));
    }
  }

  /**
   * Eine Schleife die durch die Vorgänger-Liste durch geht und die Vervindung vom Ziel zurück zum Start verfolgt.
   * @return Eine Liste mit gelaufenen Knoten.
   */
  public LinkedList<Knoten> findeWeg(Knoten ziel) {
    LinkedList<Knoten> weg = new LinkedList<Knoten>();
    weg.add(ziel);

    // prüfen ob eine Verbindung existiert
    if (this.vorgaenger.get(ziel) == null) {
      return null;
    }

    while (this.vorgaenger.get(ziel) != null) {
      ziel = this.vorgaenger.get(ziel);
      weg.add(ziel);
    }

    return weg;
  }


  /**
   * Gib nützliche Informationen wie Heuristik, Knoten mit ihren Kosten und gesammte Kosten auf die Konsole aus.
   */
  protected void statistikAusgeben() {
    // System.out.println("\nListe mit gelaufene Kantengewichte" + 
    //                    "\n----------------------------------");
    // this.zurueckgelegteWegeAusgeben();

    // System.out.println("\nDer finaler Weg (absteigend)" + 
    //                    "\n----------------------------------");
    // this.finalerWegAusgeben();

    System.out.println("\n" +
      "----------------------------------\n" +
      "Gesamtkosten: " + this.maximaleWeglaengeAusgeben());

    System.out.println("Schleifendurchlauf: " + this.durchgang);
  }

  /**
   * Gibt alle möglichen Wege vom Start- zum Zielpunt mit ihren Kosten zurück.
   */
  abstract void zurueckgelegteWegeAusgeben();

  /**
   * Gibt die maximale Weglänge vom Start- zum Zielpunkt aus.
   * Eine Bedingung muss abgefangen werden, wenn es kein Weg vom Start- zum Zielpunkt existiert.
   * Dann entspricht die Gesamtkosten natürlich 0.
   * @return Die maximale Weglänge.
   */
  abstract float maximaleWeglaengeAusgeben();

  /**
   * Gibt die Liste der Vorgänger auf die Konsole aus.
   */
  protected void finalerWegAusgeben() {
    int knotenbezeichnung = 0;

    for (Knoten entry : this.finalerWeg) {
      for (int i = 0; i < this.knoten.size(); i++) {
        if (this.knoten.get(i) == entry) {
          knotenbezeichnung = i;
        }
      }
      System.out.printf("Weg: %d\n", knotenbezeichnung);
    }
  }
} // ENDE Klasse Algorithmus
