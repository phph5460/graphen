/**
 * Generiert den Graph und verwaltet alle drei Algorithmen.
 */
public class Graph {

  // Globale Eigenschaften
  public static final int RADIUS = 8;
  public static final float WASSERSPIEGEL = Hoehendaten.HOCHPUNKT * 0.30;
  public static final int ANSTRENGUNGSFAKTOR = 3;


  // Graphen-Parameter
  private final int GITTERBREITE = 100;
  private final int GITTERHOEHE = 100;
  private final Knoten[] GITTERANZAHL = new Knoten[this.GITTERBREITE * this.GITTERHOEHE];
  private float seitenlaenge;
  private float seitenbreite;
  // kann nur 1 oder 2 annehmen
  private int vertikalZentrierer = 1;

  // Abhängigkeiten zu anderen Klassen
  private Algorithmus algorithmus;
  private Hoehendaten leser;
  
  private ArrayList<Knoten> knoten;
  // enthält den letzten von einem Algorithmus gefundenen Weg
  private LinkedList<Knoten> letzterWeg;

  // Start - Ziel
  private int startknoten;
  private int zielknoten;
  // für Steuerung; x-, y- Koordinate des Kreies in dem sich die Maus beim Klicken befindet
  private float kreisXKoordinate;
  private float kreisYKoordinate;


  public ArrayList<Knoten> holeKnoten() {
    return this.knoten;
  }

  public int holeStartknoten() {
    return this.startknoten;
  }

  public int holeZielknoten() {
    return this.zielknoten;
  }

  public float holeKreisXKoordinate() {
    return this.kreisXKoordinate;
  }

  public float holeKreisYKoordinate() {
    return this.kreisYKoordinate;
  }

  public int holeGitteranzahlLaenge() {
    return this.GITTERANZAHL.length;
  }

  public int holeGitterbreite() {
    return this.GITTERBREITE;
  }

  public int holeGitterhoehe() {
    return this.GITTERHOEHE;
  }

  public void setzeStartknoten(int _start) {
    this.startknoten = _start;
  }

  public void setzeZielknoten(int _ziel) {
    this.zielknoten = _ziel;
  }

  /**
   * Erzeuge die Koordinaten und Kanten des Knotens und stellt den Graph grafisch dar.
   */
  public void erzeugeGittergraph(String karte) {
    background(45, 45, 52);
    this.generiereKoordinatenGitterknoten(karte);
    this.generiereGitterkanten();
    this.zeichneGitterknoten();
  }

  /**
   * Erstellt die Position an jedem Indeces einen neuen Knoten.
   * Ziel: So groß gewählt, damit zwischen einzelnen Knoten genau ein Knoten reinpasst
   */
  public void generiereKoordinatenGitterknoten(String karte) {
    this.seitenlaenge = height / this.GITTERHOEHE;
    this.seitenbreite = (width - (this.GITTERBREITE * this.seitenlaenge)) / 2;
    float vertikalerAbstand = this.seitenlaenge / this.vertikalZentrierer;
    float horizontalerAbstand = this.seitenbreite;

    this.leser = new Hoehendaten();
    this.leser.ladeKarte(karte);

    // neue Knoten-Koordinaten über alle Indeces erstellen
    for (int hoehe = 0; hoehe < this.GITTERHOEHE; hoehe++) {
      for (int breite = 0; breite < this.GITTERBREITE; breite++) {
        this.GITTERANZAHL[hoehe * this.GITTERBREITE + breite] = new Knoten(horizontalerAbstand + (this.seitenlaenge * breite), 
                                                                           vertikalerAbstand + (this.seitenlaenge * hoehe), 
                                                                           leser.holeHoehenwerte()[hoehe * this.GITTERBREITE + breite]);
      }
    }
    this.knoten = new ArrayList<Knoten>(Arrays.asList(this.GITTERANZAHL));
  }

  private void generiereGitterkanten() {
    for (int hoehe = 0; hoehe < this.GITTERHOEHE; hoehe++) {
      for (int breite = 0; breite < this.GITTERBREITE; breite++) {
        float kantengewicht = 0;

        // Diagonale Verbindung abfangen
        // alle Kanten sollen ab dem Wasserspiegel erstellt werden
        if (breite < this.GITTERBREITE - 1 && this.istUeberDenWasserspiegel(leser.holeHoehenwerte()[hoehe * this.GITTERBREITE + breite])) {
          kantengewicht = this.hypotenuse(knoten.get(hoehe * this.GITTERBREITE + breite).xKoordinate(), 
                                              knoten.get(hoehe * this.GITTERBREITE + breite).yKoordinate(),
                                              leser.holeHoehenwerte()[hoehe * this.GITTERBREITE + breite],
                                              knoten.get(hoehe * this.GITTERBREITE + breite + 1).xKoordinate(), 
                                              knoten.get(hoehe * this.GITTERBREITE + breite + 1).yKoordinate(),
                                              leser.holeHoehenwerte()[hoehe * this.GITTERBREITE + breite + 1]);



          // bidirektionale Verbindungskanten erstellen in der Horizontale
          knoten.get(hoehe * GITTERBREITE + breite).erstelleKante(knoten.get(hoehe * this.GITTERBREITE + breite + 1), kantengewicht);
          knoten.get(hoehe * GITTERBREITE + breite + 1).erstelleKante(knoten.get(hoehe * this.GITTERBREITE + breite), kantengewicht);

          // horizontalen Linie darstellen
          stroke(200, 200, 200, 50);
          line(knoten.get(hoehe * this.GITTERBREITE + breite).xKoordinate(), knoten.get(hoehe * this.GITTERBREITE + breite).yKoordinate(), knoten.get(hoehe * this.GITTERBREITE + breite + 1).xKoordinate(), knoten.get(hoehe * this.GITTERBREITE + breite + 1).yKoordinate());
        }

        // vertikale Verbindungen in der letzten Zeile abfangen
        if (hoehe < this.GITTERHOEHE - 1 && this.istUeberDenWasserspiegel(leser.holeHoehenwerte()[hoehe * this.GITTERBREITE + breite])) {
          kantengewicht = this.hypotenuse(knoten.get(hoehe * this.GITTERBREITE + breite).xKoordinate(), 
                                              knoten.get(hoehe * this.GITTERBREITE + breite).yKoordinate(), 
                                              leser.holeHoehenwerte()[hoehe * this.GITTERBREITE + breite], 
                                              knoten.get((hoehe + 1) * this.GITTERBREITE + breite).xKoordinate(), 
                                              knoten.get((hoehe + 1) * this.GITTERBREITE + breite).yKoordinate(), 
                                              leser.holeHoehenwerte()[(hoehe + 1) * this.GITTERBREITE + breite]);


          // bidirektionale Verbindungskanten erstellen in der Vertikale
          knoten.get(hoehe * this.GITTERHOEHE + breite).erstelleKante(knoten.get((hoehe + 1) * this.GITTERHOEHE + breite), kantengewicht);
          knoten.get((hoehe + 1) * this.GITTERHOEHE + breite).erstelleKante(knoten.get(hoehe * this.GITTERHOEHE + breite), kantengewicht);

          // vertikalen Linie darstellen
          stroke(200, 200, 200, 50);
          line(knoten.get(hoehe * this.GITTERHOEHE + breite).xKoordinate(), knoten.get(hoehe * this.GITTERHOEHE + breite).yKoordinate(), knoten.get((hoehe + 1) * this.GITTERHOEHE + breite).xKoordinate(), knoten.get((hoehe + 1) * this.GITTERHOEHE + breite).yKoordinate());
        }
      }
    }
  }

  /**
   * Ermittelt ob der Knoten über den Wasserspiegel befindet.
   * @return true Wenn Wert höher ist der Wasserspiegel ist
   */
  private boolean istUeberDenWasserspiegel(float ebene) {
    return ebene >= Graph.WASSERSPIEGEL; 
  }
  
  /**
   * Berechnet die längste Strecke zwischen Punkt A und Punkt B mit Hilfe vom Satz des Pythagoras.
   * Die Distanz vom startpunkt zum zielpunkt repräsentiert eine Kante  der Kathete.
   * Die Höhe ist dann die Gegenkathete.
   * @param startX  x-Koordinate vom Startpunkt
   * @param startY  y-Koordinate vom Startpunkt
   * @param startZ  z-Koordinate vom Startpunkt
   * @param zielX   x-Koordinate vom Zielpunkt
   * @param zielX   y-Koordinate vom Zielpunkt
   * @param zielX   z-Koordinate vom Zielpunkt
   */
  private float hypotenuse(float startX, float startY, float startZ, float zielX, float zielY, float zielZ) {
    return sqrt(pow(dist(startX, startY, zielX, zielY), 2) + pow(this.differenzHoeheZweierKnoten(startZ, zielZ), 2));
  }
  
  /**
   * Berechnet die Differenz der Höhe zweier Knoten. 
   * Mit der abs()-Funktion wird dabei den absoluten Wert (Vorzeichen unbehaftet) genommen.
   * ANSTRENGUNGSFAKTOR ist hier mit einberechnet, 
   *    sodass die höchste Höhe um vielfaches höher sein wird als vorher.
   *    analog dazu werden die tiefsten Stelle mäßig höher steigen
   * @param startZ  Höhe des Startpunktes
   * @param zielZ   Höhe des Zielpunktes
   */
  private float differenzHoeheZweierKnoten(float startZ, float zielZ) {
    return pow(abs(startZ - zielZ), Graph.ANSTRENGUNGSFAKTOR);
  }

  /**
   * Ermittelt, ob die Maus im Bereich eines der Knoten befindet.
   */
  public boolean mausInKreis() {
    for (Knoten k : this.knoten) {
      if (dist(k.xKoordinate(), k.yKoordinate(), mouseX, mouseY) <= Graph.RADIUS) {
        this.kreisXKoordinate = k.xKoordinate();
        this.kreisYKoordinate = k.yKoordinate();
        return true;
      }
    }
    return false;
  }

  private void zeichneGitterknoten() {
    color ultramarin = color(50, 60, 150);
    color blau = color(66, 116, 165, 95);
    color gelb = color(216, 193, 125, 75);
    color gruen = color(105, 180, 90, 65);
    color orange = color(222, 133, 58, 85);
    color rot = color(160, 40, 35, 95);
    color aktuelleFarbe = color(0);
    float mapper, wasserMapper = 0;

    for (Knoten k : this.knoten) {
      wasserMapper = map(k.zKoordinate(), leser.minHoehe(leser.holeHoehenwerte()), Graph.WASSERSPIEGEL, 0, 1);
      mapper = map(k.zKoordinate(), leser.minHoehe(leser.holeHoehenwerte()), leser.maxHoehe(leser.holeHoehenwerte()), 0, 4);
      if (wasserMapper >= 0 && wasserMapper <= 1) {
        aktuelleFarbe = lerpColor(ultramarin, blau, wasserMapper);
        fill(aktuelleFarbe);
      } else if (mapper >= 0 && mapper <= 1) {
        aktuelleFarbe = lerpColor(blau, gelb, mapper);
        fill(aktuelleFarbe);
      } else if (mapper > 1 && mapper <= 2) {
        aktuelleFarbe = lerpColor(gelb, gruen, mapper - 1);
        fill(aktuelleFarbe);
      } else if (mapper > 2 && mapper <= 3) {
        aktuelleFarbe = lerpColor(gruen, orange, mapper - 2);
        fill(aktuelleFarbe);
      } else if (mapper > 3 && mapper <= 4) {
        aktuelleFarbe = lerpColor(orange, rot, mapper - 3);
        fill(aktuelleFarbe);
      }
      noStroke();
      rect(k.xKoordinate(), k.yKoordinate(), this.seitenlaenge, this.seitenlaenge);
    }
  }
  
  /**
   * Zeichne den Weg zum Ziel
   * @param weg Die Koordinaten, um den Weg darzustellen.
   */
  public void zeichneWeg(LinkedList<Knoten> weg) {
    float[] xWerte = new float[weg.size()];
    float[] yWerte = new float[weg.size()];
    int stelle = 0;

    for (Knoten knoten : weg) {
      xWerte[stelle] = knoten.xKoordinate();
      yWerte[stelle] = knoten.yKoordinate();

      if (stelle > 0) {
        // Weghighlight
        if (key == 'a')
          stroke(215);
        else if (key == 's')
          stroke(230, 200, 0);
        else if (key == 'd')
          stroke(45, 45, 52);
        strokeWeight(4);
        line(xWerte[stelle - 1], yWerte[stelle - 1], xWerte[stelle], yWerte[stelle]);

        // neu generierte Kanten wieder in Strichstärke von 1px
        strokeWeight(1);
        stroke(0);
      }
      stelle++;
    }
  }


  // Start der Algorithmen
  public void zufaellig() {
    this.algorithmus = new Zufallsweg(knoten);
    this.algorithmus.starteAlgorithmus(knoten.get(this.startknoten), knoten.get(this.zielknoten));

    LinkedList<Knoten> weg = new LinkedList<Knoten>();

    if (this.algorithmus.findeWeg(knoten.get(this.zielknoten)) != null) {
      weg = this.algorithmus.findeWeg(knoten.get(this.zielknoten));
    }
    this.letzterWeg = weg;
    this.zeichneWeg(weg);
  }

  public void dijkstra() {
    this.algorithmus = new Dijkstra(knoten);
    this.algorithmus.starteAlgorithmus(knoten.get(this.startknoten), knoten.get(this.zielknoten));

    LinkedList<Knoten> weg = new LinkedList<Knoten>();

    if (this.algorithmus.findeWeg(knoten.get(this.zielknoten)) != null) {
      weg = this.algorithmus.findeWeg(knoten.get(this.zielknoten));
    }
    this.letzterWeg = weg;
    this.zeichneWeg(weg);
  }

  private void astern() {
    this.algorithmus = new AStern(knoten);
    this.algorithmus.starteAlgorithmus(knoten.get(this.startknoten), knoten.get(this.zielknoten));

    LinkedList<Knoten> weg = new LinkedList<Knoten>();

    if (algorithmus.findeWeg(knoten.get(this.zielknoten)) != null) {
      weg = this.algorithmus.findeWeg(knoten.get(this.zielknoten));
    }
    this.letzterWeg = weg;
    this.zeichneWeg(weg);
  }
} // Ende Klasse Graph
