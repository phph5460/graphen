import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.*; 
import java.io.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class Main extends PApplet {




public void setup() {
  
  rectMode(CENTER);
  colorMode(RGB, 255, 255, 255, 100);
  graph.erzeugeGittergraph(Hoehendaten.KARTE_1);
}

Graph graph = new Graph();

public void draw() {}
/**
 * Der schnellste Wegfindungsalgorithmus. A-Stern ist ein informierter Algorithmus, weil er die Heuristik als Hilfsmittel miteinbezieht, sodass der Algorithmus immer ein Anhaltspunkt, wo der kürzeste Weg sich befindet. 
 */
public class AStern extends Algorithmus {

  // tatsächliches Kantengewicht + Heuristik
  private Map<Knoten, Float> laufkosten;
  // tatsächliches Kantengewicht
  private Map<Knoten, Float> realeKosten;


  public AStern(ArrayList<Knoten> _knoten) {
    super(_knoten);
  }

  /**
   * Startet den A-Stern-Algorithmus.
   * @param start Startpunkt
   * @param ziel Zielpunkt
   */
  @Override
  public void starteAlgorithmus(Knoten start, Knoten ziel) {
    // Messung der Laufzeit des Algorithmus
    startNanos = System.nanoTime();
    dauerNanos = System.nanoTime() - startNanos;
    // die benötigten Liste öffnen
    open = new ArrayList<Knoten>();
    closed = new ArrayList<Knoten>();
    finalerWeg = new ArrayList<Knoten>();

    this.laufkosten = new HashMap<Knoten, Float>();
    this.realeKosten = new HashMap<Knoten, Float>();
    vorgaenger = new HashMap<Knoten, Knoten>();

    // initialisiere Startknoten
    open.add(start);
    this.laufkosten.put(start, 0.0f);
    this.realeKosten.put(start, 0.0f);
    vorgaenger.put(start, null);

    while (open.size() > 0) {
      super.durchgang++;
      // ernenne den aktuellen Knoten mit dem kleinsten Kantengewicht
      Knoten aktuellerKnoten = minimalesKantengewicht(open, this.laufkosten);
      // setz die Heuristik für das aktuelle Element
      aktuellerKnoten.setzeHeuristik(this.hypotenuse(aktuellerKnoten.xKoordinate(), 
                                                         aktuellerKnoten.yKoordinate(), 
                                                         aktuellerKnoten.zKoordinate(), 
                                                         ziel.xKoordinate(), 
                                                         ziel.yKoordinate(), 
                                                         ziel.zKoordinate()));

      if (aktuellerKnoten == ziel) {
        // geht aus der Schleife heraus
        break;
      }

      closed.add(aktuellerKnoten);
      open.remove(aktuellerKnoten);

      this.sucheNachfolger(aktuellerKnoten, ziel);
    }

    wegErstellen(vorgaenger, ziel);
    System.out.println("\n\n" +
                       "A-Stern");
    statistikAusgeben();
    System.out.println("Dauer: " + dauerNanos + " ns");
  }

  /**
   * Ermittelt alle Nachbarn des aktuellen Knotens und speichert sie in die Open-Liste.
   * @param aktuellerKnoten Pivot-Punkt, um seine Nachbarn zu ermitteln
   * @param ziel Zielpunkt des Auswahls
   */
  @Override
  public void sucheNachfolger(Knoten aktuellerKnoten, Knoten ziel) {
    for (Knoten nachbar : aktuellerKnoten.listeMitNachbarn()) {
      if (closed.contains(nachbar)) {
        // geht zum nächsten Nachbarn
        continue;
      }

      // summiert die vorherigen Kantengewichte mit dem nächsten vom Nachbar
      float realeKantengewicht = this.realeKosten.get(aktuellerKnoten) + aktuellerKnoten.holeKantengewicht(nachbar);
      // berechnet die Heuristik vom aktuellen Knoten zum Zielknoten
      float heuristik = this.hypotenuse(aktuellerKnoten.xKoordinate(), 
                                            aktuellerKnoten.yKoordinate(), 
                                            aktuellerKnoten.zKoordinate(), 
                                            ziel.xKoordinate(), 
                                            ziel.yKoordinate(), 
                                            ziel.zKoordinate());
                                        
      aktuellerKnoten.setzeHeuristik(heuristik);

      float heuristikSumme = this.realeKosten.get(aktuellerKnoten) + aktuellerKnoten.holeKantengewicht(nachbar) + aktuellerKnoten.wertDerHeuristik();

      // wenn Nachbar nicht bereits in der Open-Liste drin ist
      if (!open.contains(nachbar)) {
        // dann füge ihn hinzu
        open.add(nachbar);
        // wiederhole den Vorgang, wenn die Kosten des nächsten Nachbarns größer ist
      } else if (heuristikSumme >= this.laufkosten.get(nachbar)) {
        continue;
      } else if (realeKantengewicht >= this.realeKosten.get(nachbar)) {
        continue;
      }

      vorgaenger.put(nachbar, aktuellerKnoten);
      this.laufkosten.put(nachbar, heuristikSumme);
      this.realeKosten.put(nachbar, realeKantengewicht);
    }
  }
    
  /**
   * Berechnet die längste Strecke zwischen Punkt A und Punkt B mit Hilfe vom Satz des Pythagoras.
   * Die Distanz vom startpunkt zum zielpunkt repräsentiert eine Kante  der Kathete.
   * Die Höhe ist dann die Gegenkathete.
   * @param startX  x-Koordinate vom Startpunkt
   * @param startY  y-Koordinate vom Startpunkt
   * @param startZ  z-Koordinate vom Startpunkt
   * @param zielX   x-Koordinate vom Zielpunkt
   * @param zielX   y-Koordinate vom Zielpunkt
   * @param zielX   z-Koordinate vom Zielpunkt
   */
  private float hypotenuse(float startX, float startY, float startZ, float zielX, float zielY, float zielZ) {
    return sqrt(pow(dist(startX, startY, zielX, zielY), 2) + pow(this.differenzHoeheZweierKnoten(startZ, zielZ), 2));
  }
  
  /**
   * Berechnet die Differenz der Höhe zweier Knoten. 
   * Mit der abs()-Funktion wird dabei den absoluten Wert (Vorzeichen unbehaftet) genommen.
   * @param startZ  Höhe des Startpunktes
   * @param zielZ   Höhe des Zielpunktes
   */
  private float differenzHoeheZweierKnoten(float startZ, float zielZ) {
   return abs(startZ - zielZ);
  }

  /**
   * Gibt alle möglichen Wege vom Start- zum Zielpunt mit ihren Kosten zurück.
   */
  @Override
  public void zurueckgelegteWegeAusgeben() {
    int bezeichnung = 0;

    for (Map.Entry<Knoten, Float> entry : this.realeKosten.entrySet()) {
      for (int i = 0; i < knoten.size(); i++) {
        if (knoten.get(i) == entry.getKey()) {
          bezeichnung = i;
        }
      }
      System.out.println("Knoten: " + bezeichnung + " | Kosten: " + entry.getValue());
    }
  }

  /**
   * Gibt die maximale Weglänge vom Start- zum Zielpunkt aus.
   * Eine Bedingung muss abgefangen werden, wenn es kein Weg vom Start- zum Zielpunkt existiert.
   * Dann entspricht die Gesamtkosten natürlich 0.
   * @return Die maximale Weglänge.
   */
  @Override
  public float maximaleWeglaengeAusgeben() {
    float zwischenGewicht = Float.MIN_VALUE;

    for (Knoten aktuellerKnoten : finalerWeg) {
      if (!this.realeKosten.containsKey(aktuellerKnoten)) {
        zwischenGewicht = 0.0f;
      } else if (this.realeKosten.get(aktuellerKnoten) >= zwischenGewicht) {
        zwischenGewicht = this.realeKosten.get(aktuellerKnoten);
      }
    }
    return zwischenGewicht;
  }
} // ENDE Klasse AStern
/**
 * Die Elternklasse aller Algorithmen. Hier werden standard Methoden und Eigenschaften definiert.
 */
public abstract class Algorithmus {
  
  protected final List<Knoten> knoten;
  protected ArrayList<Knoten> open;
  protected ArrayList<Knoten> closed;
  protected ArrayList<Knoten> finalerWeg;

  protected HashMap<Knoten, Knoten> vorgaenger;
  // für Schleifendurchgänge des Algorithmus
  protected int durchgang;
  // Zeitmessung in Nanosekunde
  protected long startNanos, dauerNanos;

  
  public Algorithmus(ArrayList<Knoten> _knoten) {
    this.knoten = _knoten;
  }

  /**
   * Startet den Algorithmus
   * @param start Startpunkt
   * @param ziel Zielpunkt
   */
  public abstract void starteAlgorithmus(Knoten start, Knoten ziel);

  /**
   * Ermittelt alle Nachbarn des aktuellen Knotens und speichert sie in die Open-Liste.
   * @param aktuellerKnoten Pivot-Punkt, um seine Nachbarn zu ermitteln
   */
  public abstract void sucheNachfolger(Knoten aktuellerKnoten, Knoten ziel);

  /**
   * Vergleicht verfügbare Knoten in der Open-Liste und wertet die Kosten aus, sodass der Knoten mit dem kleinsten Kantengewicht zurückgegeben wird.
   * @param listeMitKnoten Open-Liste wird mitgegeben
   * @param _kosten Knoten mit ihren Kosten
   * @return Gibt den Knoten mit dem kleinsten Kantengewicht zurück.
   */
  protected Knoten minimalesKantengewicht(ArrayList<Knoten> listeMitKnoten, Map<Knoten, Float> _kosten) {
    float zwischenGewicht = Float.MAX_VALUE;
    Knoten knoten = null;

    for (Knoten aktuellerKnoten : listeMitKnoten) {
      if (_kosten.get(aktuellerKnoten) < zwischenGewicht) {
        zwischenGewicht = _kosten.get(aktuellerKnoten);
        knoten = aktuellerKnoten;
      }
    }
    return knoten;
  }

  /**
   * Erstelle den Weg zum Ziel mit Hilfe der Vorgänger-Liste und speichert die Knoten in die finalerWeg-Liste.
   * @param _vorgaenger Die Liste mit allen gelaufenen Vorgängern.
   * @param ziel Gibt den Zielknoten an.
   */
  protected void wegErstellen(HashMap<Knoten, Knoten> _vorgaenger, Knoten ziel) {
    if (ziel != null) {
      this.finalerWeg.add(ziel);
      this.wegErstellen(_vorgaenger, _vorgaenger.get(ziel));
    }
  }

  /**
   * Eine Schleife die durch die Vorgänger-Liste durch geht und die Vervindung vom Ziel zurück zum Start verfolgt.
   * @return Eine Liste mit gelaufenen Knoten.
   */
  public LinkedList<Knoten> findeWeg(Knoten ziel) {
    LinkedList<Knoten> weg = new LinkedList<Knoten>();
    weg.add(ziel);

    // prüfen ob eine Verbindung existiert
    if (this.vorgaenger.get(ziel) == null) {
      return null;
    }

    while (this.vorgaenger.get(ziel) != null) {
      ziel = this.vorgaenger.get(ziel);
      weg.add(ziel);
    }

    return weg;
  }


  /**
   * Gib nützliche Informationen wie Heuristik, Knoten mit ihren Kosten und gesammte Kosten auf die Konsole aus.
   */
  protected void statistikAusgeben() {
    // System.out.println("\nListe mit gelaufene Kantengewichte" + 
    //                    "\n----------------------------------");
    // this.zurueckgelegteWegeAusgeben();

    // System.out.println("\nDer finaler Weg (absteigend)" + 
    //                    "\n----------------------------------");
    // this.finalerWegAusgeben();

    System.out.println("\n" +
      "----------------------------------\n" +
      "Gesamtkosten: " + this.maximaleWeglaengeAusgeben());

    System.out.println("Schleifendurchlauf: " + this.durchgang);
  }

  /**
   * Gibt alle möglichen Wege vom Start- zum Zielpunt mit ihren Kosten zurück.
   */
  public abstract void zurueckgelegteWegeAusgeben();

  /**
   * Gibt die maximale Weglänge vom Start- zum Zielpunkt aus.
   * Eine Bedingung muss abgefangen werden, wenn es kein Weg vom Start- zum Zielpunkt existiert.
   * Dann entspricht die Gesamtkosten natürlich 0.
   * @return Die maximale Weglänge.
   */
  public abstract float maximaleWeglaengeAusgeben();

  /**
   * Gibt die Liste der Vorgänger auf die Konsole aus.
   */
  protected void finalerWegAusgeben() {
    int knotenbezeichnung = 0;

    for (Knoten entry : this.finalerWeg) {
      for (int i = 0; i < this.knoten.size(); i++) {
        if (this.knoten.get(i) == entry) {
          knotenbezeichnung = i;
        }
      }
      System.out.printf("Weg: %d\n", knotenbezeichnung);
    }
  }
} // ENDE Klasse Algorithmus
/*
 * Ein schneller Wegfindungsalgorithmus, der den kleinsten Kantengewicht vom Start- zum Zielpunkt auswählt.
 */
public class Dijkstra extends Algorithmus {

  private Map<Knoten, Float> kosten;


  public Dijkstra(ArrayList<Knoten> _knoten) {
    super(_knoten);
  }

  /**
   * Startet den Dijkstra-Algorithmus.
   * @param start Startpunkt
   * @param ziel Zielpunkt
   */
  @Override
  public void starteAlgorithmus(Knoten start, Knoten ziel) {
    // Messung der Laufzeit des Algorithmus
    startNanos = System.nanoTime();
    dauerNanos = System.nanoTime() - startNanos;
    // die benötigten Liste öffnen
    open = new ArrayList<Knoten>();
    closed = new ArrayList<Knoten>();
    finalerWeg = new ArrayList<Knoten>();

    this.kosten = new HashMap<Knoten, Float>();
    vorgaenger = new HashMap<Knoten, Knoten>();

    // initialisiere Startknoten
    open.add(start);
    this.kosten.put(start, 0.0f);
    vorgaenger.put(start, null);

    // sobalb es mind. einen Knoten in der Open-Liste gibt
    while (open.size() > 0) {
      super.durchgang++;
      // ernenne den aktuellen Knoten mit dem kleinsten Kantengewicht
      Knoten aktuellerKnoten = minimalesKantengewicht(open, this.kosten);

      if (aktuellerKnoten == ziel) {
        // geht aus der Schleife heraus
        break;
      }

      closed.add(aktuellerKnoten);
      open.remove(aktuellerKnoten);

      this.sucheNachfolger(aktuellerKnoten, ziel);
    }

    wegErstellen(vorgaenger, ziel);
    System.out.println("\n\n" +
                       "Dijkstra");
    statistikAusgeben();
    System.out.println("Dauer: " + dauerNanos + " ns");
  }

  /**
   * Ermittelt alle Nachbarn des aktuellen Knotens und speichert sie in die Open-Liste.
   * @param aktuellerKnoten Pivot-Punkt, um seine Nachbarn zu ermitteln
   * @param ziel Zielpunkt des Auswahls
   */
  @Override
  public void sucheNachfolger(Knoten aktuellerKnoten, Knoten ziel) {
    for (Knoten nachbar : aktuellerKnoten.listeMitNachbarn()) {
      if (closed.contains(nachbar)) {
        // geht zum nächsten Nachbarn
        continue;
      }

      // summiert den vorherigen Kosten mit dem nächsten vom Nachbar
      float neueKosten = this.kosten.get(aktuellerKnoten) + aktuellerKnoten.holeKantengewicht(nachbar);

      // füge alle Nachbarn hinzu, die nicht in der Open-Liste drin sind
      if (!open.contains(nachbar)) {
        // füge ihn hinzu
        open.add(nachbar);
        // wiederhole den Vorgang, wenn Kosten größer, gleich ist
      } else if (neueKosten >= this.kosten.get(nachbar)) {
        continue;
      }

      vorgaenger.put(nachbar, aktuellerKnoten);
      this.kosten.put(nachbar, neueKosten);
    }
  }

  /**
   * Gibt alle möglichen Wege vom Start- zum Zielpunt mit ihren Kosten zurück.
   */
  @Override
  public void zurueckgelegteWegeAusgeben() {
    int bezeichnung = 0;

    for (Map.Entry<Knoten, Float> entry : this.kosten.entrySet()) {
      for (int i = 0; i < knoten.size(); i++) {
        if (knoten.get(i) == entry.getKey()) {
          bezeichnung = i;
        }
      }
      System.out.println("Knoten: " + bezeichnung + " | Kosten: " + entry.getValue());
    }
  }

  /**
   * Gibt die maximale Weglänge vom Start- zum Zielpunkt aus.
   * Eine Bedingung muss abgefangen werden, wenn es kein Weg vom Start- zum Zielpunkt existiert.
   * Dann entspricht die Gesamtkosten natürlich 0.
   * @return Die maximale Weglänge.
   */
  @Override
  public float maximaleWeglaengeAusgeben() {
    float zwischenGewicht = Float.MIN_VALUE;

    for (Knoten aktuellerKnoten : finalerWeg) {
      if (!this.kosten.containsKey(aktuellerKnoten)) {
        zwischenGewicht = 0.0f;
      } else if (this.kosten.get(aktuellerKnoten) >= zwischenGewicht) {
        zwischenGewicht = this.kosten.get(aktuellerKnoten);
      }
    }
    return zwischenGewicht;
  }
} // Ende Klasse Dijkstra
/**
 * Generiert den Graph und verwaltet alle drei Algorithmen.
 */
public class Graph {

  // Globale Eigenschaften
  public static final int RADIUS = 8;
  public static final float WASSERSPIEGEL = Hoehendaten.HOCHPUNKT * 0.30f;
  public static final int ANSTRENGUNGSFAKTOR = 3;


  // Graphen-Parameter
  private final int GITTERBREITE = 100;
  private final int GITTERHOEHE = 100;
  private final Knoten[] GITTERANZAHL = new Knoten[this.GITTERBREITE * this.GITTERHOEHE];
  private float seitenlaenge;
  private float seitenbreite;
  // kann nur 1 oder 2 annehmen
  private int vertikalZentrierer = 1;

  // Abhängigkeiten zu anderen Klassen
  private Algorithmus algorithmus;
  private Hoehendaten leser;
  
  private ArrayList<Knoten> knoten;
  // enthält den letzten von einem Algorithmus gefundenen Weg
  private LinkedList<Knoten> letzterWeg;

  // Start - Ziel
  private int startknoten;
  private int zielknoten;
  // für Steuerung; x-, y- Koordinate des Kreies in dem sich die Maus beim Klicken befindet
  private float kreisXKoordinate;
  private float kreisYKoordinate;


  public ArrayList<Knoten> holeKnoten() {
    return this.knoten;
  }

  public int holeStartknoten() {
    return this.startknoten;
  }

  public int holeZielknoten() {
    return this.zielknoten;
  }

  public float holeKreisXKoordinate() {
    return this.kreisXKoordinate;
  }

  public float holeKreisYKoordinate() {
    return this.kreisYKoordinate;
  }

  public int holeGitteranzahlLaenge() {
    return this.GITTERANZAHL.length;
  }

  public int holeGitterbreite() {
    return this.GITTERBREITE;
  }

  public int holeGitterhoehe() {
    return this.GITTERHOEHE;
  }

  public void setzeStartknoten(int _start) {
    this.startknoten = _start;
  }

  public void setzeZielknoten(int _ziel) {
    this.zielknoten = _ziel;
  }

  /**
   * Erzeuge die Koordinaten und Kanten des Knotens und stellt den Graph grafisch dar.
   */
  public void erzeugeGittergraph(String karte) {
    background(45, 45, 52);
    this.generiereKoordinatenGitterknoten(karte);
    this.generiereGitterkanten();
    this.zeichneGitterknoten();
  }

  /**
   * Erstellt die Position an jedem Indeces einen neuen Knoten.
   * Ziel: So groß gewählt, damit zwischen einzelnen Knoten genau ein Knoten reinpasst
   */
  public void generiereKoordinatenGitterknoten(String karte) {
    this.seitenlaenge = height / this.GITTERHOEHE;
    this.seitenbreite = (width - (this.GITTERBREITE * this.seitenlaenge)) / 2;
    float vertikalerAbstand = this.seitenlaenge / this.vertikalZentrierer;
    float horizontalerAbstand = this.seitenbreite;

    this.leser = new Hoehendaten();
    this.leser.ladeKarte(karte);

    // neue Knoten-Koordinaten über alle Indeces erstellen
    for (int hoehe = 0; hoehe < this.GITTERHOEHE; hoehe++) {
      for (int breite = 0; breite < this.GITTERBREITE; breite++) {
        this.GITTERANZAHL[hoehe * this.GITTERBREITE + breite] = new Knoten(horizontalerAbstand + (this.seitenlaenge * breite), 
                                                                           vertikalerAbstand + (this.seitenlaenge * hoehe), 
                                                                           leser.holeHoehenwerte()[hoehe * this.GITTERBREITE + breite]);
      }
    }
    this.knoten = new ArrayList<Knoten>(Arrays.asList(this.GITTERANZAHL));
  }

  private void generiereGitterkanten() {
    for (int hoehe = 0; hoehe < this.GITTERHOEHE; hoehe++) {
      for (int breite = 0; breite < this.GITTERBREITE; breite++) {
        float kantengewicht = 0;

        // Diagonale Verbindung abfangen
        // alle Kanten sollen ab dem Wasserspiegel erstellt werden
        if (breite < this.GITTERBREITE - 1 && this.istUeberDenWasserspiegel(leser.holeHoehenwerte()[hoehe * this.GITTERBREITE + breite])) {
          kantengewicht = this.hypotenuse(knoten.get(hoehe * this.GITTERBREITE + breite).xKoordinate(), 
                                              knoten.get(hoehe * this.GITTERBREITE + breite).yKoordinate(),
                                              leser.holeHoehenwerte()[hoehe * this.GITTERBREITE + breite],
                                              knoten.get(hoehe * this.GITTERBREITE + breite + 1).xKoordinate(), 
                                              knoten.get(hoehe * this.GITTERBREITE + breite + 1).yKoordinate(),
                                              leser.holeHoehenwerte()[hoehe * this.GITTERBREITE + breite + 1]);



          // bidirektionale Verbindungskanten erstellen in der Horizontale
          knoten.get(hoehe * GITTERBREITE + breite).erstelleKante(knoten.get(hoehe * this.GITTERBREITE + breite + 1), kantengewicht);
          knoten.get(hoehe * GITTERBREITE + breite + 1).erstelleKante(knoten.get(hoehe * this.GITTERBREITE + breite), kantengewicht);

          // horizontalen Linie darstellen
          stroke(200, 200, 200, 50);
          line(knoten.get(hoehe * this.GITTERBREITE + breite).xKoordinate(), knoten.get(hoehe * this.GITTERBREITE + breite).yKoordinate(), knoten.get(hoehe * this.GITTERBREITE + breite + 1).xKoordinate(), knoten.get(hoehe * this.GITTERBREITE + breite + 1).yKoordinate());
        }

        // vertikale Verbindungen in der letzten Zeile abfangen
        if (hoehe < this.GITTERHOEHE - 1 && this.istUeberDenWasserspiegel(leser.holeHoehenwerte()[hoehe * this.GITTERBREITE + breite])) {
          kantengewicht = this.hypotenuse(knoten.get(hoehe * this.GITTERBREITE + breite).xKoordinate(), 
                                              knoten.get(hoehe * this.GITTERBREITE + breite).yKoordinate(), 
                                              leser.holeHoehenwerte()[hoehe * this.GITTERBREITE + breite], 
                                              knoten.get((hoehe + 1) * this.GITTERBREITE + breite).xKoordinate(), 
                                              knoten.get((hoehe + 1) * this.GITTERBREITE + breite).yKoordinate(), 
                                              leser.holeHoehenwerte()[(hoehe + 1) * this.GITTERBREITE + breite]);


          // bidirektionale Verbindungskanten erstellen in der Vertikale
          knoten.get(hoehe * this.GITTERHOEHE + breite).erstelleKante(knoten.get((hoehe + 1) * this.GITTERHOEHE + breite), kantengewicht);
          knoten.get((hoehe + 1) * this.GITTERHOEHE + breite).erstelleKante(knoten.get(hoehe * this.GITTERHOEHE + breite), kantengewicht);

          // vertikalen Linie darstellen
          stroke(200, 200, 200, 50);
          line(knoten.get(hoehe * this.GITTERHOEHE + breite).xKoordinate(), knoten.get(hoehe * this.GITTERHOEHE + breite).yKoordinate(), knoten.get((hoehe + 1) * this.GITTERHOEHE + breite).xKoordinate(), knoten.get((hoehe + 1) * this.GITTERHOEHE + breite).yKoordinate());
        }
      }
    }
  }

  /**
   * Ermittelt ob der Knoten über den Wasserspiegel befindet.
   * @return true Wenn Wert höher ist der Wasserspiegel ist
   */
  private boolean istUeberDenWasserspiegel(float ebene) {
    return ebene >= Graph.WASSERSPIEGEL; 
  }
  
  /**
   * Berechnet die längste Strecke zwischen Punkt A und Punkt B mit Hilfe vom Satz des Pythagoras.
   * Die Distanz vom startpunkt zum zielpunkt repräsentiert eine Kante  der Kathete.
   * Die Höhe ist dann die Gegenkathete.
   * @param startX  x-Koordinate vom Startpunkt
   * @param startY  y-Koordinate vom Startpunkt
   * @param startZ  z-Koordinate vom Startpunkt
   * @param zielX   x-Koordinate vom Zielpunkt
   * @param zielX   y-Koordinate vom Zielpunkt
   * @param zielX   z-Koordinate vom Zielpunkt
   */
  private float hypotenuse(float startX, float startY, float startZ, float zielX, float zielY, float zielZ) {
    return sqrt(pow(dist(startX, startY, zielX, zielY), 2) + pow(this.differenzHoeheZweierKnoten(startZ, zielZ), 2));
  }
  
  /**
   * Berechnet die Differenz der Höhe zweier Knoten. 
   * Mit der abs()-Funktion wird dabei den absoluten Wert (Vorzeichen unbehaftet) genommen.
   * ANSTRENGUNGSFAKTOR ist hier mit einberechnet, 
   *    sodass die höchste Höhe um vielfaches höher sein wird als vorher.
   *    analog dazu werden die tiefsten Stelle mäßig höher steigen
   * @param startZ  Höhe des Startpunktes
   * @param zielZ   Höhe des Zielpunktes
   */
  private float differenzHoeheZweierKnoten(float startZ, float zielZ) {
    return pow(abs(startZ - zielZ), Graph.ANSTRENGUNGSFAKTOR);
  }

  /**
   * Ermittelt, ob die Maus im Bereich eines der Knoten befindet.
   */
  public boolean mausInKreis() {
    for (Knoten k : this.knoten) {
      if (dist(k.xKoordinate(), k.yKoordinate(), mouseX, mouseY) <= Graph.RADIUS) {
        this.kreisXKoordinate = k.xKoordinate();
        this.kreisYKoordinate = k.yKoordinate();
        return true;
      }
    }
    return false;
  }

  private void zeichneGitterknoten() {
    int ultramarin = color(50, 60, 150);
    int blau = color(66, 116, 165, 95);
    int gelb = color(216, 193, 125, 75);
    int gruen = color(105, 180, 90, 65);
    int orange = color(222, 133, 58, 85);
    int rot = color(160, 40, 35, 95);
    int aktuelleFarbe = color(0);
    float mapper, wasserMapper = 0;

    for (Knoten k : this.knoten) {
      wasserMapper = map(k.zKoordinate(), leser.minHoehe(leser.holeHoehenwerte()), Graph.WASSERSPIEGEL, 0, 1);
      mapper = map(k.zKoordinate(), leser.minHoehe(leser.holeHoehenwerte()), leser.maxHoehe(leser.holeHoehenwerte()), 0, 4);
      if (wasserMapper >= 0 && wasserMapper <= 1) {
        aktuelleFarbe = lerpColor(ultramarin, blau, wasserMapper);
        fill(aktuelleFarbe);
      } else if (mapper >= 0 && mapper <= 1) {
        aktuelleFarbe = lerpColor(blau, gelb, mapper);
        fill(aktuelleFarbe);
      } else if (mapper > 1 && mapper <= 2) {
        aktuelleFarbe = lerpColor(gelb, gruen, mapper - 1);
        fill(aktuelleFarbe);
      } else if (mapper > 2 && mapper <= 3) {
        aktuelleFarbe = lerpColor(gruen, orange, mapper - 2);
        fill(aktuelleFarbe);
      } else if (mapper > 3 && mapper <= 4) {
        aktuelleFarbe = lerpColor(orange, rot, mapper - 3);
        fill(aktuelleFarbe);
      }
      noStroke();
      rect(k.xKoordinate(), k.yKoordinate(), this.seitenlaenge, this.seitenlaenge);
    }
  }
  
  /**
   * Zeichne den Weg zum Ziel
   * @param weg Die Koordinaten, um den Weg darzustellen.
   */
  public void zeichneWeg(LinkedList<Knoten> weg) {
    float[] xWerte = new float[weg.size()];
    float[] yWerte = new float[weg.size()];
    int stelle = 0;

    for (Knoten knoten : weg) {
      xWerte[stelle] = knoten.xKoordinate();
      yWerte[stelle] = knoten.yKoordinate();

      if (stelle > 0) {
        // Weghighlight
        if (key == 'a')
          stroke(215);
        else if (key == 's')
          stroke(230, 200, 0);
        else if (key == 'd')
          stroke(45, 45, 52);
        strokeWeight(4);
        line(xWerte[stelle - 1], yWerte[stelle - 1], xWerte[stelle], yWerte[stelle]);

        // neu generierte Kanten wieder in Strichstärke von 1px
        strokeWeight(1);
        stroke(0);
      }
      stelle++;
    }
  }


  // Start der Algorithmen
  public void zufaellig() {
    this.algorithmus = new Zufallsweg(knoten);
    this.algorithmus.starteAlgorithmus(knoten.get(this.startknoten), knoten.get(this.zielknoten));

    LinkedList<Knoten> weg = new LinkedList<Knoten>();

    if (this.algorithmus.findeWeg(knoten.get(this.zielknoten)) != null) {
      weg = this.algorithmus.findeWeg(knoten.get(this.zielknoten));
    }
    this.letzterWeg = weg;
    this.zeichneWeg(weg);
  }

  public void dijkstra() {
    this.algorithmus = new Dijkstra(knoten);
    this.algorithmus.starteAlgorithmus(knoten.get(this.startknoten), knoten.get(this.zielknoten));

    LinkedList<Knoten> weg = new LinkedList<Knoten>();

    if (this.algorithmus.findeWeg(knoten.get(this.zielknoten)) != null) {
      weg = this.algorithmus.findeWeg(knoten.get(this.zielknoten));
    }
    this.letzterWeg = weg;
    this.zeichneWeg(weg);
  }

  private void astern() {
    this.algorithmus = new AStern(knoten);
    this.algorithmus.starteAlgorithmus(knoten.get(this.startknoten), knoten.get(this.zielknoten));

    LinkedList<Knoten> weg = new LinkedList<Knoten>();

    if (algorithmus.findeWeg(knoten.get(this.zielknoten)) != null) {
      weg = this.algorithmus.findeWeg(knoten.get(this.zielknoten));
    }
    this.letzterWeg = weg;
    this.zeichneWeg(weg);
  }
} // Ende Klasse Graph
/**
 * Liest externe Höhendaten ein und stellt die Daten für weitere Nutzung zur Verfügung.
 */
public class Hoehendaten {

  public static final String KARTE_1 = "data/z1.txt";
  public static final String KARTE_2 = "data/z2.txt";
  public static final String KARTE_3 = "data/z3.txt";
  public static final String KARTE_4 = "data/z4.txt";
  public static final String KARTE_5 = "data/z5.txt";
  public static final String BIG_MAP = "data/zBig.txt";
  public static final float HOCHPUNKT = 100;
  public static final float TIEFPUNKT = 0;

  // aktuell gemappte Höhenwert
  private float[] hoehenwerte;
  private float[] urspungswerte;


  public float[] holeHoehenwerte() {
    return this.hoehenwerte;
  }


  /**
   * Lädt die Höhendaten ein.
   */
  public void ladeKarte(String karte) {
    this.hoehenWertAbspeichern(karte);
    this.mappeUrspungswerte();
  }

  /**
   * Lädt die Höhendaten aus einer externen Quelle und separiert die Werte nach Komma.
   * Die Daten werden als String abgespeichert.
   */
  private void hoehenWertAbspeichern(String karte) {
    BufferedReader bufReader = null;
    String gleitkommazahl = null;
    String[] hoehenwerteInString = null;

    try {
      //bufReader = new BufferedReader(new FileReader(dataPath("z1.txt")));
      bufReader = createReader(karte);
      while ((gleitkommazahl = bufReader.readLine()) != null) {
        hoehenwerteInString = split(gleitkommazahl, ",");
      }
      
      this.uebertrageStringInFloatArray(hoehenwerteInString);
    } catch (IOException e) {
      e.printStackTrace();
    } catch (NullPointerException e) {
      e.printStackTrace();
    } finally {
      try {
        if (bufReader != null) {
          bufReader.close();
        }
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }

  /**
   * Übertrage die Strings-Höhenwerte Indeces-weise in ein Float-Array.
   * Übertrüft vorher, ob die Werte valide sind.
   * @param werteInString Zuübergenes String-Array
   */
  private void uebertrageStringInFloatArray(String[] werteInString) {
    this.urspungswerte = new float[werteInString.length];

    for (int i = 0; i < werteInString.length; i++) {  
      // akzeptiertes Wort: einstellige Ziffer "\\d", gefolgt von einem Punkt [.], dann weitere Ziffer "\\d", ein oder mehr "+"
      if (werteInString[i].matches("\\d[.]\\d+")) {
        this.urspungswerte[i] = Float.parseFloat(werteInString[i]);
      } else {
        this.urspungswerte[i] = 0.0f;
      }
    }
  }

  /**
   * Streckt die Ursprungshöhen in ein neues Intervall aus.
   */
  private void mappeUrspungswerte() {
    this.hoehenwerte = new float[this.urspungswerte.length];

    for (int i = 0; i < this.urspungswerte.length; i++) {
      this.hoehenwerte[i] = map(this.urspungswerte[i], this.minHoehe(this.urspungswerte), this.maxHoehe(this.urspungswerte), Hoehendaten.TIEFPUNKT, Hoehendaten.HOCHPUNKT);
    }
  }

  /**
   * Gib die Minimumhöhe aus.
   * @param eingangshoehe Ausgangsarray mit Höhenwerten
   */
  public float minHoehe(float[] eingangshoehe) {
    float minimum = Float.MAX_VALUE;

    for (int i = 0; i < eingangshoehe.length; i++) {
      if (eingangshoehe[i] < minimum) {
        minimum = eingangshoehe[i];
      }
    }
    return minimum;
  }

  /**
   * Gibt die Maximumhöhe aus.
   * @param eingangshoehe Ausgangsarray mit Höhenwerten
   */
  public float maxHoehe(float[] eingangshoehe) {
    float maximum = Float.MIN_VALUE;

    for (int i = 0; i < eingangshoehe.length; i++) {
      if (eingangshoehe[i] > maximum) {
        maximum = eingangshoehe[i];
      }
    }
    return maximum;
  }
} // Ende Klasse Hoehendaten
/**
 * Die Klasse Knoten beinhaltet die Koordinaten und Heuristik der einzelnen Knoten.
 * Jede Knoten sollte seine Nachbar und seine Kantengewichte kennen.
 */
public class Knoten {

  // Eigenschaften der Klasse-Knoten
  private float xPosition;
  private float yPosition;
  private float zPosition;
  private float heuristik;

  private HashMap<Knoten, Float> kanten = new HashMap<Knoten, Float>();
  private ArrayList<Knoten> nachbarn = new ArrayList<Knoten>();


  public Knoten(float _xPos, float _yPos, float _zPos) {
    this.xPosition = _xPos;
    this.yPosition = _yPos;
    this.zPosition = _zPos;
  }

  public float xKoordinate() {
    return this.xPosition;
  }

  public float yKoordinate() {
    return this.yPosition;
  }
  
  public float zKoordinate() {
    return this.zPosition; 
  }

  public float wertDerHeuristik() {
    return this.heuristik;
  }

  public ArrayList<Knoten> listeMitNachbarn() {
    return this.nachbarn;
  }

  public void setzeHeuristik(float _heuristik) {
    this.heuristik = _heuristik;
  }


  public String toString() {
    System.out.println();
    return "\nKnoten: x: " + this.xPosition + " | y: " + this.yPosition + "  | z: " + this.zPosition;
  }

  /**
   * Liefert das Kantengewicht vom Start- zum Endknoten zurück.
   * Bsp.: Startknoten -> Endknoten (param: ende)
   @param ende Endknoten
   */
  public float holeKantengewicht(Knoten ende) {
    return this.kanten.get(ende);
  }

  /**
   * Ziel: Alle Knoten sollen seine Nachbarn kennen.
   * Erstellt die Kanten aus der bereits verfügbaren Knoten.
   * @param Startknoten
   * @param kantengewicht das dazugehörige Kantengewicht
   */
  private void erstelleKante(Knoten knoten, float kantengewicht) {
    this.kanten.put(knoten, kantengewicht);
    this.nachbarn.add(knoten);
  }
} // ENDE Klasse Knoten
/*
 *
 *                        Steuerung der Höhenkarte
 *
 */

// max. 2 (Start- und Zielknoten), um das Markieren weiterer Knoten und andere mögliche Bugs zu verhindern
private int anzahlAusgewaehlterKnoten = 0; 
// noch kein Start- und Zielknoten ausgewählt
private boolean startzustand = false;
private boolean zielzustand = false;
// Index des Startknotens der vorherigen Suche
private int indexVorherigerStartknoten = 0;
private int indexVorherigerEndknoten = 0;
// Ziffer die der Position des Knoten in "knoten" entspricht
private int knotenziffer = 0; 

/**
 * Steuerungsmethode Processing
 * wird nach dem Drücken der linken Maustaste ausgeführt
 * hier: Knotenauswahl
 * Startknoten: Knoten, von welchem aus sie Wegsuche beginnen soll
 * Endknoten: Knoten, welcher durch die Wegsuche erreicht werden soll
 */
public void mousePressed() {
  // wenn sich der Mauszeiger in einem Kreis befindet 
  if (graph.mausInKreis()) {

    // Mehrfachauswahl; falls nötig eine erneute Wegsuche auf dem Graphen vorbereiten
    neueAuswahlErmoeglichen();

    // Knotenziffer zunächst wieder auf 0 setzen
    this.knotenziffer = 0;

    // Auswahl Startknoten
    startknotenAuswaehlen();

    // Auswahl Endknoten
    endknotenAuswaehlen();
  }

  // falls Start- und Zielknoten beide gesetzt sind 
  if (startzustand == true && zielzustand == true) {
    startzustand = false;
    zielzustand = false;
  }
}

/**
 * Belegt die Taste von 1-5 mit verschiedenen Karten.
 * [A] = A-Stern, [S] = Zufall, [D] = Dijkstra
 * mit [LEERTASTE] wird die Karte zurückgesetzt
 */
public void keyPressed() {
  switch (key) {
    case '1':
      this.resetAuswahl();
      graph.erzeugeGittergraph(Hoehendaten.KARTE_1);
    break;
    case '2':
      this.resetAuswahl();
      graph.erzeugeGittergraph(Hoehendaten.KARTE_2);
    break;
    case '3':
      this.resetAuswahl();
      graph.erzeugeGittergraph(Hoehendaten.KARTE_3);
    break;
    case '4':
      this.resetAuswahl();
      graph.erzeugeGittergraph(Hoehendaten.KARTE_4);
    break;
    case '5':
      this.resetAuswahl();
      graph.erzeugeGittergraph(Hoehendaten.KARTE_5);
    break;
    case '0':
      this.resetAuswahl();
      graph.erzeugeGittergraph(Hoehendaten.BIG_MAP);
    break;
    // Fälle für Algorithmen
    case 'a':
      if (anzahlAusgewaehlterKnoten == 2) {
        graph.astern();
      }
    break;
    case 's':
      if (anzahlAusgewaehlterKnoten == 2) {
        graph.zufaellig();
      }
    break;
    case 'd':
      if (anzahlAusgewaehlterKnoten == 2) {
        graph.dijkstra();
      }
    break;
    case ' ':
      this.resetAuswahl();
      background(45, 45, 52);
      graph.generiereGitterkanten();
      graph.zeichneGitterknoten();
    break;
    default:
      System.out.println("\n" +
                         "Die gedrückte Taste ist nicht belegt.\n" + 
                         "Drücke [1], [2], [3], [4] oder [5] für die Auswahl der Karte.\n" +
                         "Drücke [LEERTASTE] für Reset der Karte.\n" +
                         "[A]: AStern, [S]: Zuzfall, [D]: Dijsktra");
  }
}

/**
 * Bevor ein neuer Graph erstellt wird, Startknoten, Zielknoten und Anzahl wieder auf 0 setzen 
 */
private void resetAuswahl() {
  graph.setzeStartknoten(0);
  graph.setzeZielknoten(0);
  this.anzahlAusgewaehlterKnoten = 0;
  startzustand = false;
  zielzustand = false;
}

/**
 * Methode ermöglicht eine erneute Wegsuche auf dem selben Graphen.
 */
private void neueAuswahlErmoeglichen() {
  // falls die Anzahl der ausgewaehlten Knoten = 2 ist
  if (this.anzahlAusgewaehlterKnoten == 2) {
    // Knoten und Kanten erneut zeichnen
    background(45, 45, 52);
    graph.generiereGitterkanten();
    graph.zeichneGitterknoten();

    // Anzahl der ausgewaehlten Knoten wieder auf 0 setzen
    this.anzahlAusgewaehlterKnoten = 0;
  }
}

/**
 * Methode zum Setzen und Markieren des Startknotens
 */
private void startknotenAuswaehlen() {
  // falls noch kein Stratknoten ausgewählt wurde
  if (startzustand == false) {
    // über die Ziffern aller Knoten des Graphen iterieren
    for (int i = 0; i < graph.holeGitteranzahlLaenge(); i++) {
      // falls die X- und Y-Koordinaten des Kreises in dem sich die Maus beim Klicken befindet den Koordinaten des Knotens an der Stelle i in "knoten" entsprechen
      if (graph.holeKreisXKoordinate() == graph.holeKnoten().get(i).xKoordinate() && graph.holeKreisYKoordinate() == graph.holeKnoten().get(i).yKoordinate()) {
        // Knoten an Stelle [i] wird durch Kreis in welchem geklickt wurde beschrieben
        this.knotenziffer = i;
        this.indexVorherigerStartknoten = i;
      }
    }

    // setze den Knoten an der Stelle [i] als Startknoten
    graph.setzeStartknoten(knotenziffer);
    System.out.println("\n##################################\n" + 
                       "Stratknoten Nr.: " + graph.holeStartknoten());
    // Startknoten gesetzt/ausgewählt
    startzustand = true;
    // daher Anzahl ausgewaehlter Knoten erhöhen
    this.anzahlAusgewaehlterKnoten++;

    // Start-Markierung zeichnen
    fill(25, 130, 55); // grün
    strokeWeight(2);
    stroke(255);
    ellipse(graph.holeKreisXKoordinate(), graph.holeKreisYKoordinate(), Graph.RADIUS * 2, Graph.RADIUS * 2);
    strokeWeight(1);
  }
}

/**
 * Methode zum Setzen und Markieren des Endknotens
 */
private void endknotenAuswaehlen() {
  // falls ein Startknoten aber noch kein Zielknoten ausgewählt wurde
  if (startzustand == true && zielzustand == false) {
    // über die Ziffern aller Knoten des Graphen iterieren
    for (int i = 0; i < graph.holeGitteranzahlLaenge(); i++) {
      // falls die X- und Y-Koordinatne des Kreises in dem sich die Maus beim Klicken befindet den Koordinaten des Knotens an der Stelle i in "knoten" entsprechen
      if (graph.holeKreisXKoordinate() == graph.holeKnoten().get(i).xKoordinate() && graph.holeKreisYKoordinate() == graph.holeKnoten().get(i).yKoordinate()) {
        // Knoten an Stelle [i] wird durch Kreis in welchem geklickt wurde beschrieben
        this.knotenziffer = i;
        this.indexVorherigerEndknoten = i;
      }
    }

    // falls Knoten an Stelle [i] nicht schon als Startknoten gewählt wurde
    if (this.knotenziffer != graph.holeStartknoten()) {
      // setze Knoten an Stelle [i] als Zielknoten
      graph.setzeZielknoten(this.knotenziffer);
      println("Zielknoten Nr.: " + graph.holeZielknoten());
      // Zielknoten gesetzt/ausgewählt
      zielzustand = true;
      // daher Anzahl ausgewaehlter Knoten erhöhen
      this.anzahlAusgewaehlterKnoten++;

      // Ziel-Markierung zeichnen
      fill(255, 0, 20); // rot
      strokeWeight(2);
      stroke(255);
      ellipse(graph.holeKreisXKoordinate(), graph.holeKreisYKoordinate(), Graph.RADIUS * 2, Graph.RADIUS * 2);
      strokeWeight(1);
    }
  }
}
/**
 * Ein Wegfindungsalgorithmus, der zufällig den Weg vom Start- zum Zielpunkt auswählt, ohne dabei im Kreis zu laufen.
 */
public class Zufallsweg extends Algorithmus {

  private Map<Knoten, Float> kosten;


  public Zufallsweg(ArrayList<Knoten> _knoten) {
    super(_knoten);
  }

  /**
   * Startet den Zufalls-Algorithmus.
   * @param start Startpunkt
   * @param ziel Zielpunkt
   */
  @Override
  public void starteAlgorithmus(Knoten start, Knoten ziel) {
    // Messung der Laufzeit des Algorithmus
    startNanos = System.nanoTime();
    dauerNanos = System.nanoTime() - startNanos;
    // die benötigten Liste öffnen
    open = new ArrayList<Knoten>();
    closed = new ArrayList<Knoten>();
    finalerWeg = new ArrayList<Knoten>();

    this.kosten = new HashMap<Knoten, Float>();
    vorgaenger = new HashMap<Knoten, Knoten>();

    // initialisiere Startknoten
    open.add(start);
    this.kosten.put(start, 0.0f);
    vorgaenger.put(start, null);

    // sobalb es mind. einen Knoten in der Open-Liste gibt
    while (open.size() > 0) {
      super.durchgang++;
      // ernenne den aktuellen Knoten zufällig aus der Open-Liste
      Knoten aktuellerKnoten = this.holeZufall(open);

      if (aktuellerKnoten == ziel) {
        // geht aus der Schleife heraus
        break;
      }

      closed.add(aktuellerKnoten);
      open.remove(aktuellerKnoten);

      this.sucheNachfolger(aktuellerKnoten, ziel);
    }

    wegErstellen(vorgaenger, ziel);
    System.out.println("\n\n" +
                       "Zufallsweg");
    statistikAusgeben();
    System.out.println("Dauer: " + dauerNanos + " ns");
  }

  /**
   * Ermittelt alle Nachbarn des aktuellen Knotens und speichert sie in die Open-Liste.
   * @param aktuellerKnoten Pivot-Punkt, um seine Nachbarn zu ermitteln
   */
  @Override
  public void sucheNachfolger(Knoten aktuellerKnoten, Knoten ziel) {
    for (Knoten nachbar : aktuellerKnoten.listeMitNachbarn()) {
      if (closed.contains(nachbar)) {
        // geht zum nächsten Nachbarn
        continue;
      }

      // summiert den vorherigen Kosten mit dem nächsten vom Nachbar
      float neueKosten = this.kosten.get(aktuellerKnoten) + aktuellerKnoten.holeKantengewicht(nachbar);

      // füge alle Nachbarn hinzu, die nicht in der Open-Liste drin sind
      if (!open.contains(nachbar)) {
        // füge ihn hinzu
        open.add(nachbar);
        // wiederhole den Vorgang, wenn Kosten größer, gleich ist
      } else if (neueKosten >= this.kosten.get(nachbar)) {
        continue;
      }

      vorgaenger.put(nachbar, aktuellerKnoten);
      this.kosten.put(nachbar, neueKosten);
    }
  }
  
  /**
   * Hole aus der zuübergebene Liste zufällig den nächsten Knoten heraus.
   * @param listeMitKnoten Zuübergebene Liste mit Knoten
   * @return Gibt einen zufälligen Knoten zurück.
   */
  private Knoten holeZufall(ArrayList<Knoten> listeMitKnoten) {
    int zufall = (int)random(0, listeMitKnoten.size());
    Knoten zufallsKnoten = null;
    int i = 0;

    for (Knoten aktuellerKnoten : listeMitKnoten) {
      if (i == zufall) {
        zufallsKnoten = aktuellerKnoten;
      }
      i++;
    }
    return zufallsKnoten;
  }

  /**
   * Gibt alle möglichen Wege vom Start- zum Zielpunt mit ihren Kosten zurück.
   */
  @Override
  public void zurueckgelegteWegeAusgeben() {
    int bezeichnung = 0;

    for (Map.Entry<Knoten, Float> entry : this.kosten.entrySet()) {
      for (int i = 0; i < knoten.size(); i++) {
        if (knoten.get(i) == entry.getKey()) {
          bezeichnung = i;
        }
      }
      System.out.println("Knoten: " + bezeichnung + " | Kosten: " + entry.getValue());
    }
  }

  /**
   * Gibt die maximale Weglänge vom Start- zum Zielpunkt aus.
   * Eine Bedingung muss abgefangen werden, wenn es kein Weg vom Start- zum Zielpunkt existiert.
   * Dann entspricht die Gesamtkosten natürlich 0.
   * @return Die maximale Weglänge.
   */
  @Override
  public float maximaleWeglaengeAusgeben() {
    float zwischenGewicht = Float.MIN_VALUE;

    for (Knoten aktuellerKnoten : finalerWeg) {
      if (!this.kosten.containsKey(aktuellerKnoten)) {
        zwischenGewicht = 0.0f;
      } else if (this.kosten.get(aktuellerKnoten) >= zwischenGewicht) {
        zwischenGewicht = this.kosten.get(aktuellerKnoten);
      }
    }
    return zwischenGewicht;
  }
} // Ende Klasse Zufallsweg
  public void settings() {  fullScreen(); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "Main" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
