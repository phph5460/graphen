/*
 * Ein schneller Wegfindungsalgorithmus, der den kleinsten Kantengewicht vom Start- zum Zielpunkt auswählt.
 */
public class Dijkstra extends Algorithmus {

  private Map<Knoten, Float> kosten;


  public Dijkstra(ArrayList<Knoten> _knoten) {
    super(_knoten);
  }

  /**
   * Startet den Dijkstra-Algorithmus.
   * @param start Startpunkt
   * @param ziel Zielpunkt
   */
  @Override
  public void starteAlgorithmus(Knoten start, Knoten ziel) {
    // Messung der Laufzeit des Algorithmus
    startNanos = System.nanoTime();
    dauerNanos = System.nanoTime() - startNanos;
    // die benötigten Liste öffnen
    open = new ArrayList<Knoten>();
    closed = new ArrayList<Knoten>();
    finalerWeg = new ArrayList<Knoten>();

    this.kosten = new HashMap<Knoten, Float>();
    vorgaenger = new HashMap<Knoten, Knoten>();

    // initialisiere Startknoten
    open.add(start);
    this.kosten.put(start, 0.0);
    vorgaenger.put(start, null);

    // sobalb es mind. einen Knoten in der Open-Liste gibt
    while (open.size() > 0) {
      super.durchgang++;
      // ernenne den aktuellen Knoten mit dem kleinsten Kantengewicht
      Knoten aktuellerKnoten = minimalesKantengewicht(open, this.kosten);

      if (aktuellerKnoten == ziel) {
        // geht aus der Schleife heraus
        break;
      }

      closed.add(aktuellerKnoten);
      open.remove(aktuellerKnoten);

      this.sucheNachfolger(aktuellerKnoten, ziel);
    }

    wegErstellen(vorgaenger, ziel);
    System.out.println("\n\n" +
                       "Dijkstra");
    statistikAusgeben();
    System.out.println("Dauer: " + dauerNanos + " ns");
  }

  /**
   * Ermittelt alle Nachbarn des aktuellen Knotens und speichert sie in die Open-Liste.
   * @param aktuellerKnoten Pivot-Punkt, um seine Nachbarn zu ermitteln
   * @param ziel Zielpunkt des Auswahls
   */
  @Override
  public void sucheNachfolger(Knoten aktuellerKnoten, Knoten ziel) {
    for (Knoten nachbar : aktuellerKnoten.listeMitNachbarn()) {
      if (closed.contains(nachbar)) {
        // geht zum nächsten Nachbarn
        continue;
      }

      // summiert den vorherigen Kosten mit dem nächsten vom Nachbar
      float neueKosten = this.kosten.get(aktuellerKnoten) + aktuellerKnoten.holeKantengewicht(nachbar);

      // füge alle Nachbarn hinzu, die nicht in der Open-Liste drin sind
      if (!open.contains(nachbar)) {
        // füge ihn hinzu
        open.add(nachbar);
        // wiederhole den Vorgang, wenn Kosten größer, gleich ist
      } else if (neueKosten >= this.kosten.get(nachbar)) {
        continue;
      }

      vorgaenger.put(nachbar, aktuellerKnoten);
      this.kosten.put(nachbar, neueKosten);
    }
  }

  /**
   * Gibt alle möglichen Wege vom Start- zum Zielpunt mit ihren Kosten zurück.
   */
  @Override
  public void zurueckgelegteWegeAusgeben() {
    int bezeichnung = 0;

    for (Map.Entry<Knoten, Float> entry : this.kosten.entrySet()) {
      for (int i = 0; i < knoten.size(); i++) {
        if (knoten.get(i) == entry.getKey()) {
          bezeichnung = i;
        }
      }
      System.out.println("Knoten: " + bezeichnung + " | Kosten: " + entry.getValue());
    }
  }

  /**
   * Gibt die maximale Weglänge vom Start- zum Zielpunkt aus.
   * Eine Bedingung muss abgefangen werden, wenn es kein Weg vom Start- zum Zielpunkt existiert.
   * Dann entspricht die Gesamtkosten natürlich 0.
   * @return Die maximale Weglänge.
   */
  @Override
  public float maximaleWeglaengeAusgeben() {
    float zwischenGewicht = Float.MIN_VALUE;

    for (Knoten aktuellerKnoten : finalerWeg) {
      if (!this.kosten.containsKey(aktuellerKnoten)) {
        zwischenGewicht = 0.0;
      } else if (this.kosten.get(aktuellerKnoten) >= zwischenGewicht) {
        zwischenGewicht = this.kosten.get(aktuellerKnoten);
      }
    }
    return zwischenGewicht;
  }
} // Ende Klasse Dijkstra
